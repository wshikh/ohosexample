var led = {open:1,close:0,change:2}
import app from '@system.app';
export default {
    data: {
        title: "",
        statu:'0'
    },
    onInit() {
        this.title = this.$t('strings.world');
    },
    ledon(e) {
        let that = this
        console.info("ledon")
        app.ledcontrol({
            code:led.open,
            success(res){
                that.statu = res.led_status
            },
            fail(res,code){
                console.error("ledon error")
            },
            complete(){
                console.info("ledon complete")
            }
        })
    },
    ledoff(e) {
        let that = this
        console.info("ledoff")
        app.ledcontrol({
            code:led.close,
            success(res){
                that.statu = res.led_status
            },
            fail(res,code){
                console.error("ledoff error")
            },
            complete(){
                console.info("ledoff complete")
            }
        })
    },
    ledtoggle(e) {
        let that = this
        console.info("ledtoggle")
        app.ledcontrol({
            code:led.change,
            success(res){
                that.statu = res.led_status
            },
            fail(res,code){
                console.error("ledtoggle failed")
            },
            complete(){
                console.info("ledtoggle complete")
            }
        })
    },
    exit(e) {
        app.terminate()
    },

}
