
export class ConfigData {

    TAG='hrt_Test '

    WH_25_100 = '25%';
    WH_30_100 = '30%';
    WH_33_100 = '33%';
    WH_35_100 = '35%';
    WH_40_100 = '40%';
    WH_45_100 = '45%';
    WH_50_100 = '50%';
    WH_55_100 = '55%';
    WH_60_100 = '60%';
    WH_66_100 = '66%';
    WH_70_100 = '70%';
    WH_80_100 = '80%';
    WH_83_100 = '83%';
    WH_90_100 = '90%';
    WH_100_100 ='100%';

    value_20 = 20;
    font_20 = 20;

    MAX_LINES_1 = 1;
    MAX_LINES_2 = 2;
    MAX_LINES_3 = 3;
    DURATION_TIME = 200;
    FUNCTION_TYPE_HDC = 4;
}

let configData = new ConfigData();
export default configData as ConfigData;